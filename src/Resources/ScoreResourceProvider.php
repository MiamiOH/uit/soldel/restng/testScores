<?php

namespace MiamiOH\RestngTestScores\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ScoreResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'TestScores.Response',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Unique ID of the stduent. For POST or PUT, either uniqueId or pidm must be provided.'
                ),
                'pidm' => array(
                    'type' => 'integer',
                    'required' => false,
                    'description' => 'Pidm of the stduent.For POST or PUT, either uniqueId or pidm must be provided.'
                ),
                'testCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The actual test code stored in Banner'
                ),
                'testType' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Test type, GRE/SAT/MPT, etc. It is the category of the test code, and it is not a Banner field. Only the API knows about it, in order to offer a more end-user-friendly querying and filtering option. '
                ),
                'testScore' => array('type' => 'string', 'required' => false, 'description' => 'Score of the test'),
                'testDate' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'Date of the test taken in the format of yyyy-mm-dd.'
                ),
                'tsrcCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The source, or how the test score was entered.'
                ),
                'equivInd' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'Indicator if the test score is an equivalency for the actual test taken. .'
                ),
                'tadmType' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The administrative type of a particular test. '
                ),
                'admrCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The admissions checklist item code. '
                ),
                'teinCode' => array('type' => 'string', 'required' => false, 'description' => 'Test instrument code. '),
                'tefrCode' => array('type' => 'string', 'required' => false, 'description' => 'Test form code.'),
                'teacCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Test accomodation code.'
                ),
                'teprCode' => array('type' => 'string', 'required' => false, 'description' => 'Test purpose code. '),
                'rcrvInd' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Indicates whether SAT score has been Recentered and/or Revised. '
                ),
                'satOrigScore' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The original SAT score.'
                ),
                'termCodeEntry' => array('type' => 'string', 'required' => false, 'description' => 'No descriptions.'),
                'applNo' => array('type' => 'integer', 'required' => false, 'description' => 'No descriptions.'),
                'instrID' => array('type' => 'string', 'required' => false, 'description' => 'No descriptions.'),
                'releaseInd' => array('type' => 'string', 'required' => false, 'description' => 'No descriptions.'),
                'satEssayID' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'SAT Essay ID to view the prospects essay on the SAT website.'
                ),
                'userID' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'User ID. User who inserted or last updated the data.'
                ),
                'dataOrigin' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Data Origin. Source system that created or updated the row.'
                ),

            ),
        ));

        $this->addDefinition(array(
            'name' => 'TestScores.Response.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/TestScores.Response'
            )
        ));

        $this->addDefinition(array(
            'name' => 'TestScores.Post.Request',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'Unique ID of the student.'
                ),
                'testCode' => array('type' => 'string', 'required' => true, 'description' => 'The Banner test code.'),
                'testScore' => array('type' => 'string', 'required' => true, 'description' => 'Score of the test.'),
                'testDate' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'Date of the test taken in the format of yyyy-mm-dd.'
                ),
                'tsrcCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The source, or how the test score was entered. Check stvtsrc for valid values. '
                ),
                'equivInd' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'Indicator if the test score is an equivalency for the actual test taken. Allowed values:[Y,N].'
                ),
                'tadmType' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The administrative type of a particular test. Check stvadmr for valid values.'
                ),
                'admrCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The admissions checklist item code. Check stvadmr table for valid values.'
                ),
                'teinCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Test instrument code. Check stvtein table for valid values.'
                ),
                'tefrCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Test form code. Check stvtefr table for valid values.'
                ),
                'teacCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Test accomodation code. Check stvteac table for valid values.'
                ),
                'teprCode' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Test purpose code. Check stvtepr table for valid values.'
                ),
                'rcrvInd' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'Indicates whether SAT score has been Recentered and/or Revised. Allowed values:[Y,N].'
                ),
                'satOrigScore' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'The original SAT score.'
                ),
                'termCodeEntry' => array('type' => 'string', 'required' => false, 'description' => 'No descriptions.'),
                'applNo' => array('type' => 'integer', 'required' => false, 'description' => 'No descriptions.'),
                'instrID' => array('type' => 'string', 'required' => false, 'description' => 'No descriptions.'),
                'releaseInd' => array('type' => 'string', 'required' => false, 'description' => 'No descriptions.'),
                'satEssayID' => array(
                    'type' => 'string',
                    'required' => false,
                    'description' => 'SAT Essay ID to view the prospects essay on the SAT website.'
                ),
                // 'userID'=> array('type' => 'string', 'required' => false,'description' =>'User ID. User who inserted or last updated the data.'),
                // 'dataOrigin'=> array('type' => 'string', 'required' => false,'description' =>'Data Origin. Source system that created or updated the row.')
            ),
        ));

        $this->addDefinition(array(
            'name' => 'TestScores.ID',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'Unique ID of the stduent.'
                ),
                'testCode' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'The actual test code stored in Banner'
                ),
                'testDate' => array(
                    'type' => 'string',
                    'required' => true,
                    'description' => 'Date of the test taken in the format of yyyy-mm-dd.'
                ),
            ),
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'TestScores\Score',
            'class' => 'MiamiOH\RestngTestScores\Services\Score',
            'description' => 'Provide API access for test scores.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
                'helper' => array('type' => 'service', 'name' => 'TestScores\Helper'),
            ),
        ));

        $this->addService(array(
            'name' => 'TestScores\Helper',
            'class' => 'MiamiOH\RestngTestScores\Services\Helper',
            'description' => 'Utility to help validation, building queries, tranlsating test types, etc.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'testScores.v1.muid',
            'description' => 'Get test scores for one user.',
            'pattern' => '/testScores/v1/:muid',
            'service' => 'TestScores\Score',
            'method' => 'getScoresForOne',
            'returnType' => 'collection',
            'params' => array(
                'muid' => array('description' => 'ID of the test taker.', 'alternateKeys' => ['uniqueId', 'pidm']),
            ),
            'options' => array(
                'testType' => array(
                    'type' => 'list',
                    'description' => 'A comma-separated list of test types. e.g: GRE|SAT|MPT.',
                    'enum' => ['MPT', 'GRE']
                ),
                'testCode' => array(
                    'type' => 'list',
                    'description' => 'A comma-separated list of test codes. e.g: GR02|MPT1.'
                ),
                'testDate' => array(
                    'type' => 'single',
                    'description' => 'Test date in the format of yyyy-mm-dd. e.g:2016-09-02.'
                ),
                'testDateAfter' => array(
                    'type' => 'single',
                    'description' => 'Tests after a certain date in the format of yyyy-mm-dd. e.g:2016-09-02.'
                ),
                'testDateBefore' => array(
                    'type' => 'single',
                    'description' => 'Tests before a certain date in the format of yyyy-mm-dd. e.g:2016-09-02.'
                ),

            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Read Response',
                    'returns' => array(
                        'type' => 'model',
                        'schema' => array(
                            '$ref' => '#/definitions/TestScores.Response.Collection',
                        )
                    )
                )
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'TestScores',
                        'key' => array('all', 'view'),
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'uid',
                    )
                )
            )

        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'testScores.v1',
            'description' => 'Get test scores using filters.',
            'pattern' => '/testScores/v1',
            'service' => 'TestScores\Score',
            'method' => 'getScores',
            'returnType' => 'collection',

            'isPageable' => true,
            'defaultPageLimit' => 20,
            'maxPageLimit' => 500,

            'options' => array(
                'uniqueId' => array(
                    'type' => 'list',
                    'description' => 'A comma-separated list of unique ids of students.'
                ),
                'testType' => array(
                    'type' => 'list',
                    'description' => 'A comma-separated list of test types. e.g: GRE|SAT|MPT.',
                    'enum' => ['MPT', 'GRE']
                ),
                'testCode' => array(
                    'type' => 'list',
                    'description' => 'A comma-separated list of test codes. e.g: GR02|MPT1.'
                ),
                'testDate' => array(
                    'type' => 'single',
                    'description' => 'Test date in the format of yyyy-mm-dd. e.g:2016-09-02.'
                ),
                'testDateAfter' => array(
                    'type' => 'single',
                    'description' => 'Tests after a certain date in the format of yyyy-mm-dd. e.g:2016-09-02.'
                ),
                'testDateBefore' => array(
                    'type' => 'single',
                    'description' => 'Tests before a certain date in the format of yyyy-mm-dd. e.g:2016-09-02.'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Read Response',
                    'returns' => array(
                        'type' => 'model',
                        'schema' => array(
                            '$ref' => '#/definitions/TestScores.Response.Collection',
                        )
                    )
                )
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'TestScores',
                    'key' => array('all', 'view')
                ),
            ),


        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'testScores.v1.uid.create',
            'description' => 'Create a test score record for a student',
            'pattern' => '/testScores/v1',
            'service' => 'TestScores\Score',
            'method' => 'createScore',
            'returnType' => 'model',
            'body' => array(
                'description' => 'Test score object(s)',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/TestScores.Post.Request',
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'POST Response',
                    'returns' => array(
                        'type' => 'model',
                        'schema' => array(
                            '$ref' => '#/definitions/TestScores.Response',
                        )
                    )
                ),
                App::API_BADREQUEST => array(
                    'description' => 'Some or all data are bad.',
                ),
                App::API_FAILED => array(
                    'description' => 'Insert operation failed.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'TestScores',
                    'key' => array('all', 'create')
                ),
            ),

        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'testScores.v1.update',
            'description' => 'Update a test score record for a student',
            'pattern' => '/testScores/v1',
            'service' => 'TestScores\Score',
            'method' => 'updateScore',
            'returnType' => 'model',
            'body' => array(
                'description' => 'Test score object(s)',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/TestScores.Post.Request',
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'PUT Response',
                    'returns' => array(
                        'type' => 'model',
                        'schema' => array(
                            '$ref' => '#/definitions/TestScores.Response',
                        )
                    )
                ),
                App::API_BADREQUEST => array(
                    'description' => 'Some or all data are bad.',
                ),
                App::API_FAILED => array(
                    'description' => 'Insert operation failed.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'TestScores',
                    'key' => array('all', 'update')
                ),
            ),


        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'testScores.v1.delete',
            'description' => 'Delete one or multiple test score record',
            'pattern' => '/testScores/v1',
            'service' => 'TestScores\Score',
            'method' => 'deleteScore',
            'returnType' => 'none',
            //  'params' => array(
            //     'uid' => array('description' => 'Unique id of the student.'),
            // ),
            'body' => array(
                'description' => 'The combined key of the record to be deleted',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/TestScores.ID',
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Deleted successfully.',
                ),
                App::API_BADREQUEST => array(
                    'description' => 'Some or all data are bad.',
                ),
                App::API_FAILED => array(
                    'description' => 'Insert operation failed.',
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'Unauthorized access.',
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'TestScores',
                    'key' => array('all', 'delete')
                )
            ),

        ));

    }

    public function registerOrmConnections(): void
    {

    }
}