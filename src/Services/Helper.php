<?php

namespace MiamiOH\RestngTestScores\Services;

class Helper {

    private $testTypeMap;

    private $config;

    private $dbDataSourceName = 'MUWS_GEN_PROD';
    private $dbh;

    private $phpDateFormat = 'Y-m-d';

    public function setDatabase($database) {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }


    public function setConfiguration($configuration) 
    {
        $this->config = $configuration->getConfiguration('RESTng-TestScores', 'Internal Configs');
        $testTypes = explode(",",trim($this->config['test_types']));

        foreach($testTypes as $type){
            $type = trim($type);
            $codes = $this->config["test_codes_$type"];
            $this->testTypeMap[$type] = explode(",",$codes);
        }
       
    }

    public function setTestTypeMap($map){
        return $this->testTypeMap = $map;
    }

    //get test codes from type types
    //input: array of test types
    //return: arrray of test codes
    public function getTestCodes($testTypes){
        $codes = array();
        foreach($testTypes as $type){                
            if(isset($this->testTypeMap[$type])){
                $codes = array_merge($codes,$this->testTypeMap[$type]);
            }            
        }
        return $codes;

    }

    //Translate from test code to test type. E.g. CM% is a test code of the Regional Math Placement Test. Note that the test type is not stored in Banner. It only exists in the
        //web service to make them more meaningful and easier to filter on.
    //input: test code (e.g. CM2, GR02)
    //output: test type (e.g. MPT-R, GRE)
    public function getTestType($testCode){
        foreach($this->testTypeMap as $type=>$codes){
            if(in_array($testCode,$codes)) {
                return $type;
            }
        }
        return null;
    }

    //Build "in" clause for SQL queries
    public function buildInString($array,$isString){
        $result = "";
        foreach($array as $element){
            if($isString){
                $result = $result.",'".$element."'";
            }else{
                $result = $result.",".$element;
            }
        }
        $result = substr($result,1);
        return $result;

    }

    //Get regex patterns for input params
    public function getPattern($val){
        if($val == 'pidm')
            return '/^\d{1,8}$/';
        else if($val == 'uniqueId')
            return '/^\w{1,8}$/';
    }

    //validate input params based on regex
    public function validateInput($input, $pattern, $errMesg){

        if(is_array($input)){
            foreach($input as $value){
                if(!preg_match($pattern, $value)){
                    throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
                }
            }
        }
        else{
            if(!preg_match($pattern, $input)){               
                throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
            }
        }
        return true;
    }
    //build json record from the raw database record
    public function buildRecord($row) {
        $record = array(
            'pidm'=>$row['sortest_pidm'],
            'uniqueId'=>$row['szbuniq_unique_id'],
            'testCode'=>$row['sortest_tesc_code'],
            'testType'=>$this->getTestType($row['sortest_tesc_code']),
            'testDate'=>date($this->phpDateFormat,strtotime($row['sortest_test_date'])),
            'testScore'=>$row['sortest_test_score'],
            'tsrcCode'=>$row['sortest_tsrc_code'],
            'tadmCode'=>$row['sortest_tadm_code'],
            'admrCode'=>$row['sortest_admr_code'],
            'teinCode'=>$row['sortest_tein_code'],
            'tefrCode'=>$row['sortest_tefr_code'],
            'teacCode'=>$row['sortest_teac_code'],
            'teprCode'=>$row['sortest_tepr_code'],
            'rcrvInd'=>$row['sortest_rcrv_ind'],
            'satOrigScore'=>$row['sortest_sat_orig_score'],
            'termCodeEntry'=>$row['sortest_term_code_entry'],
            'applNo'=>$row['sortest_appl_no'],
            'instrID'=>$row['sortest_instr_id'],
            'releaseInd'=>$row['sortest_release_ind'],
            'equivInd'=>$row['sortest_equiv_ind'],
            'satEssayID'=>$row['sortest_sat_essay_id'],
            'userID'=>$row['sortest_user_id'],
            'dataOrigin'=>$row['sortest_data_origin'],
        // 'internalRecordId'=> $row['ROWIDTOCHAR(ROWID)']
        );

        return $record;
    }

    public function validateScoreObject($data,$context){

        if (!is_array($data)) {
            throw new \Exception("Data for $context must be an array");
        }

        if(isset($data['uniqueId'])){
            $this->validateInput(trim($data['uniqueId']),$this->getPattern("uniqueId"),"Invalid unique ID.");  
        }else{
            throw new \MiamiOH\RESTng\Exception\BadRequest("Unique ID is required.");
        }

        if(isset($data['testCode'])){
            $this->validateInput(trim($data['testCode']),'/^\w{1,6}$/',"Invalid test code.");  
        }else{
            throw new \MiamiOH\RESTng\Exception\BadRequest("TestCode is required.");
        }


        if($context == 'post_put'){

            if(isset($data['testScore'])){
                $this->validateInput(trim($data['testScore']),'/^\w{1,5}$/',"Invalid test score.");  
            }else{
                throw new \MiamiOH\RESTng\Exception\BadRequest("TestScore is required.");
            }

            if(isset($data['equivInd'])){
                $this->validateInput(trim($data['equivInd']),'/^[YN]$/',"Invalid equivalency indicator."); 
            }else{
                throw new \MiamiOH\RESTng\Exception\BadRequest("EquivInd is required.");
            }

        }

        if(isset($data['testDate'])) {        
            if(($timestamp = strtotime($data['testDate'])) === false) {
                throw new \MiamiOH\RESTng\Exception\BadRequest("Invalid testDate.");
            }else{              
                $data['testDateFinal'] = date($this->phpDateFormat,$timestamp);
            }               
        }else{
            throw new \MiamiOH\RESTng\Exception\BadRequest("TestDate is required.");
        }

        $data['pidm'] = $this->getPidm($data['uniqueId']);

        if ($data['pidm'] === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {

            throw new \MiamiOH\RESTng\Exception\BadRequest('Unique ID does not exist.');
        }
        return $data;

    }

    protected function getPidm($uniqueID){
        $pidm = $this->dbh->queryfirstcolumn('
          select szbuniq_pidm from szbuniq where upper(szbuniq_unique_id) = upper(?)
          ', $uniqueID);

        return $pidm;
    }
    



}

?>
