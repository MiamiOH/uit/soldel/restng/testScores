<?php

namespace MiamiOH\RestngTestScores\Services;

class Score extends \MiamiOH\RESTng\Service {


	private $dbDataSourceName = 'MUWS_GEN_PROD';
	private $dbh;

	private $helper;


	public function setDatabase($database) {
		$this->dbh = $database->getHandle($this->dbDataSourceName);
	}

	public function setBannerUtil($bannerUtil)
	{
	    /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
	    $this->bannerUtil = $bannerUtil;
	}   


    public function setHelper($helper){
    	$this->helper = $helper;
    }


	public function getScoresForOne() {

		$request = $this->getRequest();
		$response = $this->getResponse();


		$eid = $request->getResourceParam('muid');

		//validation
		switch ($request->getResourceParamKey('muid')) {
		  case 'uniqueId':
		        $this->helper->validateInput($eid,$this->helper->getPattern("uniqueId"),"Invalid unique ID.");  
		        break;
		  case 'pidm':
		        $this->helper->validateInput($eid,$this->helper->getPattern("pidm"),"Invalid pidm.");
		        break;
		}      

		try{
		    $person = $this->bannerUtil->getId($request->getResourceParamKey('muid'),$request->getResourceParam('muid'));
		    // $uid = $person->getUniqueId();    
		    $pidm = $person->getPidm();

		}catch(\Exception $e){
		    throw new \MiamiOH\RESTng\Exception\BadRequest($request->getResourceParamKey('muid')." does not exist.");
		}       

		//store where clauses
		$where = array("sortest_pidm=".$pidm);


		//process the query strings
		$options = $request->getOptions();       

		$result = array();

		if(isset($options['testType'])){
			$testCodes = $this->helper->getTestcodes($options['testType']);
			if(count($testCodes) > 0) {
				$where[] = "sortest_tesc_code IN (".$this->helper->buildInString($testCodes,true).")";	
			}else{
				throw new \MiamiOH\RESTng\Exception\BadRequest("Invalid test type(s).");
			}							
		}	
		if(isset($options['testCode'])){
			$this->helper->validateInput($options['testCode'],'/^\w{1,6}$/',"Invalid test code(s).");
			$testCodes = $options['testCode'];
			$where[] = "sortest_tesc_code IN (".$this->helper->buildInString($testCodes,true).")";				
		}
		if(isset($options['testDate'])){			
			$this->helper->validateInput($options['testDate'],'/^\d{4}-\d{2}-\d{2}$/',"Invalid test date format.");
			$where[] = "sortest_test_date=to_date('".$options['testDate']."','YYYY-MM-DD')";				
		}
		if(isset($options['testDateAfter'])){			
			$this->helper->validateInput($options['testDateAfter'],'/^\d{4}-\d{2}-\d{2}$/',"Invalid test date format.");
			$where[] = "sortest_test_date>to_date('".$options['testDateAfter']."','YYYY-MM-DD')";				
		}
		if(isset($options['testDateBefore'])){			
			$this->helper->validateInput($options['testDateBefore'],'/^\d{4}-\d{2}-\d{2}$/',"Invalid test date format.");
			$where[] = "sortest_test_date<to_date('".$options['testDateBefore']."','YYYY-MM-DD')";				
		}

		$whereStr = "";

		if(count($where) > 0){
			$whereStr = " AND ".implode(" AND ",$where);
		}

		$payload = array();

		$results = $this->dbh->queryall_array($this->getScoresQuery().$whereStr);
		
        foreach($results as $row){
         	$payload[] = $this->helper->buildRecord($row);
    	}       	

		$response->setPayload($payload);
		$response->setStatus(\MiamiOH\RESTng\App::API_OK);

		return $response;
	}


	//get scores using query strings as filters
	public function getScores(){

		$request = $this->getRequest();
		$response = $this->getResponse();

		$options = $request->getOptions();

		 //paging enforced
        $offset = $request->getOffset();
        $limit = $request->getLimit();   

		$where = array();

		if(isset($options['uniqueId'])){
            $uids = $options['uniqueId']; 
            //$this->helper->validateInput($uids,$this->helper->getPattern("uniqueId"),"Invalid Unique ID.");

            $pidms = array();
            foreach($uids as $uid){
	          	try{
	          		$this->helper->validateInput($uid,$this->helper->getPattern("uniqueId"),"Invalid Unique ID.");
			    	$person = $this->bannerUtil->getId('uniqueId',$uid);
			    	$pidms[] = $person->getPidm();

				}catch(\Exception $e){
			    	// throw new \MiamiOH\RESTng\Exception\BadRequest("User $uid does not exist.");
				}
			}
			if(count($pidms) > 0){      
            	$where[] = "sortest_pidm in (".$this->helper->buildInString($pidms,false).")";
        	}            
        }
        if(isset($options['testType'])){

			$testCodes = $this->helper->getTestcodes($options['testType']);

			if(count($testCodes) > 0) {
				$where[] = "sortest_tesc_code IN (".$this->helper->buildInString($testCodes,true).")";	
			}else{
				throw new \MiamiOH\RESTng\Exception\BadRequest("Invalid test type(s).");
			}							
		}	
		if(isset($options['testCode'])){
			$this->helper->validateInput($options['testCode'],'/^\w{1,6}$/',"Invalid test code(s).");
			$testCodes = $options['testCode'];
			$where[] = "sortest_tesc_code IN (".$this->helper->buildInString($testCodes,true).")";				
		}
		if(isset($options['testDate'])){			
			$this->helper->validateInput($options['testDate'],'/^\d{4}-\d{2}-\d{2}$/',"Invalid test date format.");
			$where[] = "sortest_test_date=to_date('".$options['testDate']."','YYYY-MM-DD')";				
		}
		if(isset($options['testDateAfter'])){			
			$this->helper->validateInput($options['testDateAfter'],'/^\d{4}-\d{2}-\d{2}$/',"Invalid test date format.");
			$where[] = "sortest_test_date>to_date('".$options['testDateAfter']."','YYYY-MM-DD')";				
		}
		if(isset($options['testDateBefore'])){			
			$this->helper->validateInput($options['testDateBefore'],'/^\d{4}-\d{2}-\d{2}$/',"Invalid test date format.");
			$where[] = "sortest_test_date<to_date('".$options['testDateBefore']."','YYYY-MM-DD')";				
		}


		$whereStr = "";
		if(count($where) > 0){
			$whereStr = " AND ".implode(" AND ",$where);
		}

		$payload = array();

		$query="
        SELECT * 
        FROM (SELECT a.*, ROWNUM rnum 
            FROM ("
                .$this->getScoresQuery().$whereStr.
              " ORDER BY sortest_pidm) a
            WHERE  rownum <=".($limit+$offset-1).")
        WHERE  rnum >= ".$offset;

		$results = $this->dbh->queryall_array($query);
		
        foreach($results as $row){
         	$payload[] = $this->helper->buildRecord($row);
    	}       	

    	//get the total object number
        $query = "
        SELECT  count(*) as total
        FROM sortest, szbuniq WHERE sortest_pidm = szbuniq_pidm ".$whereStr;


        $results = $this->dbh->queryall_array($query);

        if($results[0]){
            $totalObjects = $results[0]['total'];        
        }

		$response->setPayload($payload);
        $response->setTotalObjects($totalObjects);
		$response->setStatus(\MiamiOH\RESTng\App::API_OK);

		return $response;

	}

	//baseline sql query for GET
	private function getScoresQuery(){

		$query = "SELECT sortest.*, szbuniq_unique_id FROM sortest, szbuniq WHERE sortest_pidm = szbuniq_pidm ";

		return $query;
	} 


	public function createScore(){

		$request = $this->getRequest();
		$response = $this->getResponse();
		$data = $request->getData();


	  	//validate and format the data	
		$data = $this->helper->validateScoreObject($data,"post_put");

		$data['userID'] = "API user";
		$data['dataOrigin'] = "TestScores API";


		//create the record
		try{
			$this->createScoreRecord($data);
		}catch(\Exception $e) {
			throw new \Exception("New record insertion failed.".$e->getMessage());
		
		}	

	 	//read back the new record
	 	try{
			$responseData = $this->getSingleScore($data);

		}catch(\Exception $e) {
			throw new \Exception("Querying new record failed.".$e->getMessage());
		
		}

		$response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
		$response->setPayload($responseData);

		return $response;

	}


	public function updateScore(){

		$request = $this->getRequest();
		$response = $this->getResponse();
		$data = $request->getData();	

		//validate and format the data	
		$data = $this->helper->validateScoreObject($data,"post_put");

		$data['userID'] = "API user";
		$data['dataOrigin'] = "TestScores API";

		//create the record
		try{
			$this->updateScoreRecord($data);
		}catch(\Exception $e) {
			throw new \Exception("Updating record failed.".$e->getMessage());
		
		}	
	 	//read back the new record
	 	try{
			$responseData = $this->getSingleScore($data);		
		}catch(\Exception $e) {
			throw new \Exception("Querying new record failed.".$e->getMessage());
		
		}
		$response->setStatus(\MiamiOH\RESTng\App::API_OK);
		$response->setPayload($responseData);

		return $response;

	}

	public function deleteScore(){

		$request = $this->getRequest();
		$response = $this->getResponse();
		$payload = $request->getData();

		$data = $this->helper->validateScoreObject($payload,"delete");

		try{
			$this->deleteScoreRecord($data);
		}catch(\Exception $e) {
			throw new \Exception("Deletion failed.".$e->getMessage());			
		}
			
		$response->setStatus(\MiamiOH\RESTng\App::API_OK);

		return $response;

	}


	private function getSingleScore($data){

		$query = "
			SELECT sortest.*, szbuniq_unique_id FROM sortest, szbuniq 
			WHERE sortest_pidm = szbuniq_pidm       
			AND sortest_pidm=".$data['pidm']."
			AND sortest_tesc_code='".$data['testCode']."' 
			AND sortest_test_date=to_date('".$data['testDateFinal']."', 'YYYY-MM-DD HH24:MI:SS')";	

		$results = $this->dbh->queryall_array($query);	

		if(!is_array($results) || count($results) < 1){
			throw new \Exception("Empty query result.");
		} 

	    return $this->helper->buildRecord($results[0]);	

	}

	// create a score record in Banner
	private function createScoreRecord($data){

		$query = "
		  declare
		  BEGIN
		  SB_TESTSCORE.P_CREATE(
			p_pidm => :p_pidm,
			p_tesc_code => :p_tesc_code,
			p_test_date => to_date(:p_test_date, 'YYYY-MM-DD HH24:MI:SS'),
			p_test_score =>:p_test_score,
			p_tsrc_code => :p_tsrc_code,
			p_tadm_code => :p_tadm_code,
			p_admr_code => :p_admr_code,
			p_tein_code => :p_tein_code,
			p_tefr_code => :p_tefr_code,
			p_teac_code => :p_teac_code,
			p_tepr_code => :p_tepr_code,
			p_rcrv_ind => :p_rcrv_ind,
			p_sat_orig_score => :p_sat_orig_score,
			p_term_code_entry =>   :p_term_code_entry,
			p_appl_no =>     :p_appl_no,
			p_instr_id =>     :p_instr_id,
			p_release_ind =>     :p_release_ind,
			p_equiv_ind =>   :p_equiv_ind,
			p_sat_essay_id =>   :p_sat_essay_id,
			p_user_id =>    :p_user_id,
			p_data_origin =>   :p_data_origin,
			p_rowid_out => :p_rowid_out
			);
		  END;
		  ";

		$sth = $this->dbh->prepare($query);		

		$rowID = 0;

		$sth->bind_by_name(':p_pidm', $data['pidm']);
		$sth->bind_by_name(':p_tesc_code', $data['testCode']);
		$sth->bind_by_name(':p_test_date', $data['testDateFinal']);
		$sth->bind_by_name(':p_test_score', $data['testScore']);
		$sth->bind_by_name(':p_tsrc_code', $data['tsrcCode']);
		$sth->bind_by_name(':p_tadm_code', $data['tadmCode']);
		$sth->bind_by_name(':p_admr_code', $data['admrCode']);
		$sth->bind_by_name(':p_tein_code', $data['teinCode']);
		$sth->bind_by_name(':p_tefr_code', $data['tefrCode']);
		$sth->bind_by_name(':p_teac_code', $data['teacCode']);
		$sth->bind_by_name(':p_tepr_code', $data['teprCode']);
		$sth->bind_by_name(':p_rcrv_ind', $data['rcrvInd']);
		$sth->bind_by_name(':p_sat_orig_score', $data['satOrigScore']);
		$sth->bind_by_name(':p_term_code_entry', $data['termCodeEntry']);
		$sth->bind_by_name(':p_appl_no', $data['applNo']);
		$sth->bind_by_name(':p_instr_id', $data['instrID']);
		$sth->bind_by_name(':p_release_ind', $data['releaseInd']);
		$sth->bind_by_name(':p_equiv_ind', $data['equivInd']);
		$sth->bind_by_name(':p_sat_essay_id', $data['satEssayID']);
		$sth->bind_by_name(':p_user_id', $data['userID']);
		$sth->bind_by_name(':p_data_origin', $data['dataOrigin']);
		$sth->bind_by_name(':p_rowid_out',$rowID,100);
		$sth->execute();

	}

	// update a score record in Banner. it is hidden for now. it needs more testing before being published officially.
	private function updateScoreRecord($data){

		$query = "
		  declare
		  BEGIN
		  SB_TESTSCORE.P_UPDATE(
			p_pidm => :p_pidm,
			p_tesc_code => :p_tesc_code,
			p_test_date => to_date(:p_test_date, 'YYYY-MM-DD HH24:MI:SS'),
			p_test_score =>:p_test_score,
			p_tsrc_code => :p_tsrc_code,
			p_tadm_code => :p_tadm_code,
			p_admr_code => :p_admr_code,
			p_tein_code => :p_tein_code,
			p_tefr_code => :p_tefr_code,
			p_teac_code => :p_teac_code,
			p_tepr_code => :p_tepr_code,
			p_rcrv_ind => :p_rcrv_ind,
			p_sat_orig_score => :p_sat_orig_score,
			p_term_code_entry =>   :p_term_code_entry,
			p_appl_no =>     :p_appl_no,
			p_instr_id =>     :p_instr_id,
			p_release_ind =>     :p_release_ind,
			p_equiv_ind =>   :p_equiv_ind,
			p_sat_essay_id =>   :p_sat_essay_id,
			p_user_id =>    :p_user_id,
			p_data_origin =>   :p_data_origin,
			p_rowid => :p_rowid
			); 
		  END;
		  ";

		$sth = $this->dbh->prepare($query);

		$rowID = null;

		$sth->bind_by_name(':p_pidm', $data['pidm']);
		$sth->bind_by_name(':p_tesc_code', $data['testCode']);
		$sth->bind_by_name(':p_test_date', $data['testDateFinal']);
		$sth->bind_by_name(':p_test_score', $data['testScore']);
		$sth->bind_by_name(':p_tsrc_code', $data['tsrcCode']);
		$sth->bind_by_name(':p_tadm_code', $data['tadmCode']);
		$sth->bind_by_name(':p_admr_code', $data['admrCode']);
		$sth->bind_by_name(':p_tein_code', $data['teinCode']);
		$sth->bind_by_name(':p_tefr_code', $data['tefrCode']);
		$sth->bind_by_name(':p_teac_code', $data['teacCode']);
		$sth->bind_by_name(':p_tepr_code', $data['teprCode']);
		$sth->bind_by_name(':p_rcrv_ind', $data['rcrvInd']);
		$sth->bind_by_name(':p_sat_orig_score', $data['satOrigScore']);
		$sth->bind_by_name(':p_term_code_entry', $data['termCodeEntry']);
		$sth->bind_by_name(':p_appl_no', $data['applNo']);
		$sth->bind_by_name(':p_instr_id', $data['instrID']);
		$sth->bind_by_name(':p_release_ind', $data['releaseInd']);
		$sth->bind_by_name(':p_equiv_ind', $data['equivInd']);
		$sth->bind_by_name(':p_sat_essay_id', $data['satEssayID']);
		$sth->bind_by_name(':p_user_id', $data['userID']);
		$sth->bind_by_name(':p_data_origin', $data['dataOrigin']);
		$sth->bind_by_name(':p_rowid',$rowID,100);
		$sth->execute();

	}
	private function deleteScoreRecord($data){

		$query = "
		  declare
		  BEGIN
		  SB_TESTSCORE.P_DELETE(
		  	p_pidm => :p_pidm,
			p_tesc_code => :p_tesc_code,
			p_test_date => to_date(:p_test_date, 'YYYY-MM-DD HH24:MI:SS'),
			p_rowid => :p_rowid
			); 
		  END;
		  ";

		$rowID = null;
		 
		$sth = $this->dbh->prepare($query); 
		$sth->bind_by_name(':p_pidm',$data['pidm']);
		$sth->bind_by_name(':p_tesc_code',$data['testCode']);
		$sth->bind_by_name(':p_test_date',$data['testDateFinal']);
		$sth->bind_by_name(':p_rowid',$rowID);
		$sth->execute();

	}




}
