<?php

namespace MiamiOH\RestngTestScores\Tests\Unit;

use MiamiOH\RESTng\App;

class DELETEScoreTest extends \MiamiOH\RESTng\Testing\TestCase {


    private $dbh,$sth,$request, $response, $helper, $bannerUtil, $score, $queryallRecords,$person;


    protected function setUp() {

        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
                        ->setMethods(array('getData'))
                        ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
                ->setMethods(array('queryall_array','queryfirstcolumn','prepare'))
                ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
                ->setMethods(array('getHandle'))
                ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('bind_by_name','execute'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->willReturn(true);

        $this->sth->method('execute')
            ->willReturn(true);

        $this->dbh->method('prepare')->willReturn($this->sth);
      

        $this->score = new \MiamiOH\RestngTestScores\Services\Score();

        $this->score->setApp($api);
        $this->score->setDatabase($db);
        $this->score->setBannerUtil($this->bannerUtil);
        $this->score->setRequest($this->request);

        $this->helper = new \MiamiOH\RestngTestScores\Services\Helper();

        //mock the test type mapping
        $this->testTypeMap = array('MPT'=>array('MPTR','MPT1','CM2','CM5','CM7'),'GRE'=>array('GR01','GR02','GR05'));
        $this->helper->setTestTypeMap($this->testTypeMap);
        $this->helper->setDatabase($db);

        $this->score->setHelper($this->helper);

      
      }

    
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid unique ID.     
     */
    public function testDELETEInvalidUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataInvalidUniqueID')));   
        
        $this->response  = $this->score->deleteScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Unique ID is required.
     */
    public function testDELETENotSetUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataNotSetUniqueID')));
        
        $this->response  = $this->score->deleteScore();

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid test code.     
     */
    public function testDELETEInvalidTestCode() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataInvalidTestCode')));   
        
        $this->response  = $this->score->deleteScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestCode is required.
     */
    public function testDELETENotSetTestCode() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataNotSetTestCode')));
        
        $this->response  = $this->score->deleteScore();

    }
    
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid testDate.     
     */
    public function testDELETEInvalidTestDate() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataInvalidTestDate')));   
        
        $this->response  = $this->score->deleteScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestDate is required.
     */
    public function testDELETENotSetTestDate() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataNotSetTestDate')));
        
        $this->response  = $this->score->deleteScore();

    }

 /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Unique ID does not exist.
     */

    public function testDELETENonExistUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataNonExistUniqueID')));

        $this->dbh->method('queryfirstcolumn')
               ->willReturn(\MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET);

        $this->response  = $this->score->deleteScore();
       
    }



    //A successful DELETE.
    public function testDELETESuccess(){
        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataValid')));

        $this->dbh->method('queryfirstcolumn')
               ->willReturn(123456);

        $this->response  = $this->score->deleteScore();
       
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());      
        $this->assertEquals($this->response->getPayload(), array());

    }

    //DELETE successful, but couldn't get the record back from SQL query
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Deletion failed.
     */

    public function testDELETEFailedQuery(){
        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getDELETEDataValid')));
        
        $this->dbh->method('queryfirstcolumn')
               ->willReturn(123456);

        $this->sth->method('execute')
            ->will($this->throwException(new \Exception));

        $this->response  = $this->score->deleteScore();

    }

    public function getDELETEDataInvalidUniqueID(){
        return array( 
                "uniqueId"=> 'invalid_uid@#$%',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21'
            );
    }
     public function getDELETEDataNotSetUniqueID(){
        return array( 
                // "uniqueId"=> 'invalid_uid@#$%',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getDELETEDataInvalidTestCode(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'invalid_C*)',                
                "testDate"=> '2016-08-21'
            );
    }
     public function getDELETEDataNotSetTestCode(){
        return array( 
                "uniqueId"=> 'someuid',
                // "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',                            
            );
    }
    
    public function getDELETEDataInvalidTestDate(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-13-21'//invalid
            );
    }
     public function getDELETEDataNotSetTestDate(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                // "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }

    public function getDELETEDataValid(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
            );
    }
    public function getDELETEDataNonExistUniqueID(){
        return array( 
                "uniqueId"=> 'NOBODY',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }


}
