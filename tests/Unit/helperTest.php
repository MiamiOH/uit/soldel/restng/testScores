<?php

namespace MiamiOH\RestngTestScores\Tests\Unit;

use MiamiOH\RESTng\App;

class HelperTest extends \MiamiOH\RESTng\Testing\TestCase {

    private $dbh,$helper, $response, $bannerUtil, $queryallRecords,$testTypeMap;

    protected function setUp() {

        $mockApp = $this->createMock(App::class);

        $mockApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->helper = new \MiamiOH\RestngTestScores\Services\Helper();

        //mock the test type mapping
        $this->testTypeMap = array('MPT'=>array('MPTR','MPT1','CM2','CM5','CM7'),'GRE'=>array('GR01','GR02','GR05'));
        $this->helper->setTestTypeMap($this->testTypeMap);

        $this->helper->setDatabase($db);

    }

  public function testGetTestCodes() {

    $this->assertEquals($this->helper->getTestCodes(array('MPT')),array('MPTR','MPT1','CM2','CM5','CM7'));
    $this->assertEquals($this->helper->getTestCodes(array('MPT','GRE')),array('MPTR','MPT1','CM2','CM5','CM7','GR01','GR02','GR05'));
    $this->assertEquals($this->helper->getTestCodes(array('bad_type','GRE')),array('GR01','GR02','GR05'));
    $this->assertEquals($this->helper->getTestCodes(array('bad_type','bad_type')),array());
    $this->assertEquals($this->helper->getTestCodes(array()),array());

  }

  public function testGetTestType(){
    $this->assertEquals($this->helper->getTestType('CM2'),'MPT');
    $this->assertEquals($this->helper->getTestType('CM5'),'MPT');
    $this->assertEquals($this->helper->getTestType('CM7'),'MPT');
    $this->assertEquals($this->helper->getTestType('GR01'),'GRE');
    $this->assertEquals($this->helper->getTestType('GR02'),'GRE');
    $this->assertEquals($this->helper->getTestType('bad_code'),null);
    $this->assertEquals($this->helper->getTestType(''),null);
  }

  public function testBuildInString(){
    $this->assertEquals($this->helper->buildInString(array('str1','str2','str3'),true),"'str1','str2','str3'");
    $this->assertEquals($this->helper->buildInString(array('str1','str2','str3'),false),"str1,str2,str3");
    $this->assertEquals($this->helper->buildInString(array(1,2,3),false),"1,2,3");
    $this->assertEquals($this->helper->buildInString(array('1','2','3'),false),"1,2,3");
    $this->assertEquals($this->helper->buildInString(array('1','2','3'),true),"'1','2','3'");
    $this->assertEquals($this->helper->buildInString(array(),true),'');
    $this->assertEquals($this->helper->buildInString(array(),false),'');

  }

  public function testGetPattern(){
    $this->assertEquals($this->helper->getPattern('pidm'),'/^\d{1,8}$/');
    $this->assertEquals($this->helper->getPattern('uniqueId'),'/^\w{1,8}$/');
    $this->assertEquals($this->helper->getPattern('badinput'),null);

  }


  public function testValidateInputValid(){
    $this->assertEquals($this->helper->validateInput(1225918,'/^\d{1,8}$/',"Invalid Pidm."),true);
    $this->assertEquals($this->helper->validateInput(array('gengx','abc123'),'/^\w{1,8}$/',"Invalid Unique ID."),true);

  }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
   public function testValidateInputBadInput(){
    $this->assertEquals($this->helper->validateInput('12259ab','/^\d{1,8}$/',"Invalid Pidm."),true);
   }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
   public function testValidateInputBadInputArray(){
    $this->assertEquals($this->helper->validateInput(array('12259ab',1234567),'/^\d{1,8}$/',"Invalid Pidm."),true);
   }



}
