<?php

namespace MiamiOH\RestngTestScores\Tests\Unit;

use MiamiOH\RESTng\App;

class POSTScoreTest extends \MiamiOH\RESTng\Testing\TestCase {


    private $dbh,$sth,$request, $response, $helper, $bannerUtil, $score, $queryallRecords,$person;


    protected function setUp() {

        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
                        ->setMethods(array('getData'))
                        ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
                ->setMethods(array('queryall_array','queryfirstcolumn','prepare'))
                ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
                ->setMethods(array('getHandle'))
                ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('bind_by_name','execute'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->willReturn(true);

        $this->sth->method('execute')
            ->willReturn(true);

        $this->dbh->method('prepare')->willReturn($this->sth);
      

        $this->score = new \MiamiOH\RestngTestScores\Services\Score();

        $this->score->setApp($api);
        $this->score->setDatabase($db);
        $this->score->setBannerUtil($this->bannerUtil);
        $this->score->setRequest($this->request);

        $this->helper = new \MiamiOH\RestngTestScores\Services\Helper();

        //mock the test type mapping
        $this->testTypeMap = array('MPT'=>array('MPTR','MPT1','CM2','CM5','CM7'),'GRE'=>array('GR01','GR02','GR05'));
        $this->helper->setTestTypeMap($this->testTypeMap);
        $this->helper->setDatabase($db);

        $this->score->setHelper($this->helper);

      
      }

    
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid unique ID.     
     */
    public function testPOSTInvalidUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataInvalidUniqueID')));   
        
        $this->response  = $this->score->createScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Unique ID is required.
     */
    public function testPOSTNotSetUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataNotSetUniqueID')));
        
        $this->response  = $this->score->createScore();

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid test code.     
     */
    public function testPOSTInvalidTestCode() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataInvalidTestCode')));   
        
        $this->response  = $this->score->createScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestCode is required.
     */
    public function testPOSTNotSetTestCode() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataNotSetTestCode')));
        
        $this->response  = $this->score->createScore();

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid test score.     
     */
    public function testPOSTInvalidTestScore() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataInvalidTestScore')));   
        
        $this->response  = $this->score->createScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestScore is required.
     */
    public function testPOSTNotSetTestScore() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataNotSetTestScore')));
        
        $this->response  = $this->score->createScore();

    }
     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid equivalency indicator.     
     */
    public function testPOSTInvalidEquivInd() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataInvalidEquivInd')));   
        
        $this->response  = $this->score->createScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage EquivInd is required.
     */
    public function testPOSTNotSetEquivInd() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataNotSetEquivInd')));
        
        $this->response  = $this->score->createScore();

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid testDate.     
     */
    public function testPOSTInvalidTestDate() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataInvalidTestDate')));   
        
        $this->response  = $this->score->createScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestDate is required.
     */
    public function testPOSTNotSetTestDate() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataNotSetTestDate')));
        
        $this->response  = $this->score->createScore();

    }

 /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Unique ID does not exist.
     */

    public function testPOSTNonExistUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataNonExistUniqueID')));

        $this->dbh->method('queryfirstcolumn')
               ->willReturn(\MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET);

        $this->response  = $this->score->createScore();
       
    }



    //A successful POST.
    public function testPOSTSuccess(){
        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataValid')));
        
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryPostResult')));


        $this->dbh->method('queryfirstcolumn')
               ->willReturn(123456);

        $this->response  = $this->score->createScore();
       
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $this->response->getStatus());      
        $this->assertEquals($this->response->getPayload(), $this->mockPostResponse());

    }

    //POST successful, but couldn't get the record back from SQL query
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Querying new record failed.
     */

    public function testPOSTFailedQuery(){
        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPOSTDataValid')));
        
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryPostEmptyResult')));

        $this->dbh->method('queryfirstcolumn')
               ->willReturn(123456);

        $this->response  = $this->score->createScore();

    }

    public function getPOSTDataInvalidUniqueID(){
        return array( 
                "uniqueId"=> 'invalid_uid@#$%',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPOSTDataNotSetUniqueID(){
        return array( 
                // "uniqueId"=> 'invalid_uid@#$%',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPOSTDataInvalidTestCode(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'invalid_C*)',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPOSTDataNotSetTestCode(){
        return array( 
                "uniqueId"=> 'someuid',
                // "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPOSTDataInvalidTestScore(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '7899999',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPOSTDataNotSetTestScore(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                // "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPOSTDataInvalidEquivInd(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "A", //invalid
                "satEssayID"=> ''                             
            );
    }
     public function getPOSTDataNotSetEquivInd(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                // "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPOSTDataInvalidTestDate(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-13-21',//invalid
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "Y", 
                "satEssayID"=> ''                             
            );
    }
     public function getPOSTDataNotSetTestDate(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                // "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }

    public function getPOSTDataValid(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPOSTDataNonExistUniqueID(){
        return array( 
                "uniqueId"=> 'NOBODY',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }

    public function mockQueryPostResult(){
        return array(
            array(
            'sortest_pidm' => 123456,
            'sortest_tesc_code' => 'CM5',
            'sortest_test_date' => '21-AUG-16',
            'sortest_test_score' => '78',
            'sortest_tsrc_code' => 'MUTX',
            'sortest_tadm_code' => '',
            'sortest_activity_date' => '30-AUG-16',
            'sortest_admr_code' => '',
            'sortest_tein_code' => '',
            'sortest_tefr_code' => '',
            'sortest_teac_code' => '',
            'sortest_tepr_code' => '',
            'sortest_rcrv_ind' => '',
            'sortest_sat_orig_score' =>'', 
            'sortest_term_code_entry' => '',
            'sortest_appl_no' => '',
            'sortest_instr_id' => '',
            'sortest_release_ind' => '',
            'sortest_equiv_ind' => 'N',
            'sortest_sat_essay_id' => '',
            'sortest_user_id' => '',
            'sortest_data_origin' =>'TestScores API', 
            'sortest_surrogate_id' => '',
            'sortest_version' => '',
            'sortest_vpdi_code' => '',
            'szbuniq_unique_id' => 'someuid'
           ) );
    }

    public function mockQueryPostEmptyResult(){
        return array();
    }

    public function mockPostResponse(){
        return 
            array(
                "pidm"=> 123456,
                "uniqueId"=> "someuid",
                "testCode"=> "CM5",
                "testType"=> "MPT",
                "testDate"=> "2016-08-21",
                "testScore"=> "78",
                "tsrcCode"=> "MUTX",
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> '',
                "userID"=> '',
                "dataOrigin"=> 'TestScores API'
            );
    }

}
