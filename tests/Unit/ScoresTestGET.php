<?php

namespace MiamiOH\RestngTestScores\Tests\Unit;

use MiamiOH\RESTng\App;

class ScoreTestGET extends \MiamiOH\RESTng\Testing\TestCase {


    private $dbh,$request, $response, $helper, $bannerUtil, $score, $queryallRecords,$person;

    protected function setUp() {

        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
                        ->setMethods(array('getResourceParam', 'getOptions','getResourceParamKey'))
                        ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
                ->setMethods(array('queryall_array','prepare'))
                ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
                ->setMethods(array('getHandle'))
                ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
                ->setMethods(array('getId'))
                ->getMock();

        $this->score = new \MiamiOH\RestngTestScores\Services\Score();

        $this->score->setApp($api);
        $this->score->setDatabase($db);
        $this->score->setBannerUtil($this->bannerUtil);
        $this->score->setRequest($this->request);

        $this->helper = new \MiamiOH\RestngTestScores\Services\Helper();

        //mock the test type mapping
        $this->testTypeMap = array('MPT'=>array('MPTR','MPT1','CM2','CM5','CM7'),'GRE'=>array('GR01','GR02','GR05'));
        $this->helper->setTestTypeMap($this->testTypeMap);
        $this->helper->setDatabase($db);

        $this->score->setHelper($this->helper);

        $this->person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
                ->setMethods(array('getPidm','getUniqueId'))
                ->getMock();

      }

    /***************   Tests for GetScoreForOne()      ***************/

    private function mockMUIDParamByPidm(){
        $this->request->method('getResourceParam')    
              ->with('muid')
              ->willReturn(12345678);

        $this->request->method('getResourceParamKey')    
              ->with('muid') 
            ->willReturn('pidm'); 

        $this->person->method('getPidm')
                ->willReturn(12345678);

        $this->bannerUtil->method('getId')
                ->willReturn($this->person);  

    }

    public function testGetScoresForOneByUid() {

        $this->request->method('getResourceParam')    
              ->with('muid')
              ->willReturn('UID');

        $this->request->method('getResourceParamKey')    
              ->with('muid') 
            ->willReturn('uniqueId');

        $this->person->method('getPidm')
                ->willReturn(1225918);

        $this->bannerUtil->method('getId')
                // ->with($this->anything(),$this->anything())
                ->willReturn($this->person);     

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryOne')));


        $this->response  = $this->score->getScoresForOne();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
          
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }
    public function testGetScoresForOneByPidm() {

        $this->mockMUIDParamByPidm(); 

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryOne')));

        $this->response  = $this->score->getScoresForOne();
        $payload = $this->response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }
    public function testGetScoresForOneWithValidTestTypeOption() {

        $this->mockMUIDParamByPidm();

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestTypeOptionsValid')));      

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryOne')));

        $this->response  = $this->score->getScoresForOne();
        $payload = $this->response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }
     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
     public function testGetScoresForOneWithInvalidTestTypeOption() {

        $this->mockMUIDParamByPidm();
        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestTypeOptionsInvalid')));  
        $this->response  = $this->score->getScoresForOne();

    }
    public function testGetScoresForOneWithValidTestCodeOption() {

        $this->mockMUIDParamByPidm();

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestCodeOptionsValid')));      

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryOne')));

        $this->response  = $this->score->getScoresForOne();
        $payload = $this->response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }

     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
     public function testGetScoresForOneWithInvalidTestCodeOption() {

        $this->mockMUIDParamByPidm();
        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestCodeOptionsInvalid')));  
        $this->response  = $this->score->getScoresForOne();

    }

    public function testGetScoresForOneWithValidTestDateOption() {

        $this->mockMUIDParamByPidm();

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestDateOptionsValid')));      

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryOne')));

        $this->response  = $this->score->getScoresForOne();
        $payload = $this->response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }

     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
     public function testGetScoresForOneWithInvalidTestDateOption() {

        $this->mockMUIDParamByPidm();
        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestDateOptionsInvalid')));  
        $this->response  = $this->score->getScoresForOne();

    }

    /***************   Tests for GetScores()      ***************/
    public function testGetScoresWithValidUniqueIDOption() {

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnUniqueIDOptionsValid')));  

        $this->person->method('getPidm')
                // ->willReturn(1225918);
                ->will($this->onConsecutiveCalls(123456,654321));

        $this->bannerUtil->method('getId')
                // ->with($this->anything(),$this->anything())
                ->willReturn($this->person);  

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             // ->will($this->returnCallback(array($this, 'mockQueryOne')));
             ->will($this->onConsecutiveCalls($this->returnCallback(array($this, 'mockQueryMultiple')),
             $this->returnCallback(array($this, 'mockQueryTotalScores'))));

        $this->response  = $this->score->getScores();
        $payload = $this->response->getPayload();
        $totalObj = $this->response->getTotalObjects();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryMultipleResponse());

    }

     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
    public function testGetScoresWithInvalidUniqueIDOption() {

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnUniqueIDOptionsInvalid')));  
        $this->response  = $this->score->getScores();

    }
    public function testGetScoresWithValidTestTypeOption() {

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestTypeOptionsValid')));  

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             // ->will($this->returnCallback(array($this, 'mockQueryOne')));
             ->will($this->onConsecutiveCalls($this->returnCallback(array($this, 'mockQueryOne')),
             $this->returnCallback(array($this, 'mockQueryTotalScores'))));

        $this->response  = $this->score->getScores();
        $payload = $this->response->getPayload();
        $totalObj = $this->response->getTotalObjects();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }

     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
     public function testGetScoresWithInvalidTestTypeOption() {

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestTypeOptionsInvalid')));  
        $this->response  = $this->score->getScores();

    }

    public function testGetScoresWithValidTestCodeOption() {

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestCodeOptionsValid')));  

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             // ->will($this->returnCallback(array($this, 'mockQueryOne')));
             ->will($this->onConsecutiveCalls($this->returnCallback(array($this, 'mockQueryOne')),
             $this->returnCallback(array($this, 'mockQueryTotalScores'))));

        $this->response  = $this->score->getScores();
        $payload = $this->response->getPayload();
        $totalObj = $this->response->getTotalObjects();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }

     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
     public function testGetScoresWithInvalidTestCodeOption() {

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestCodeOptionsInvalid')));  
        $this->response  = $this->score->getScores();

    }


    public function testGetScoresWithValidTestDateOption() {

         $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestDateOptionsValid')));  

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
             // ->will($this->returnCallback(array($this, 'mockQueryOne')));
             ->will($this->onConsecutiveCalls($this->returnCallback(array($this, 'mockQueryOne')),
             $this->returnCallback(array($this, 'mockQueryTotalScores'))));

        $this->response  = $this->score->getScores();
        $payload = $this->response->getPayload();
        $totalObj = $this->response->getTotalObjects();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockQueryOneResponse());

    }

     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     */
     public function testGetScoresWithInvalidTestDateOption() {

        $this->request->method('getOptions')
                ->will($this->returnCallback(array($this, 'returnTestDateOptionsInvalid')));  
        $this->response  = $this->score->getScores();

    }

    public function mockQueryTotalScores(){
        return  array(0=> array('total'=>5000));
    
    }

    public function returnTestTypeOptionsValid(){
        return array("testType"=>['MPT','GRE']);
    }

    public function returnTestTypeOptionsInvalid(){
        return array('testType'=>['MPTR','GR01']);
    } 
    public function returnTestCodeOptionsValid(){
        return array('testCode'=>['CM5','GR02']);
    }
    public function returnTestCodeOptionsInvalid(){
        return array('testCode'=>['long_string','CM5']);
    } 
    public function returnTestDateOptionsValid(){
        return array('testDate'=>'2016-08-21');
    }
    public function returnTestDateOptionsInvalid(){
        return array('testDate'=>'yyyy-mm-dd');
    } 
    public function returnUniqueIDOptionsValid(){
        return array('uniqueId'=>['UID_1','UID_2']);
    }
    public function returnUniqueIDOptionsInvalid(){
        return array('uniqueId'=>['UID','$%6334']);
    }


    public function mockQueryOne(){
        return array(
            array(
            'sortest_pidm' => 12345678,
            'sortest_tesc_code' => 'CM5',
            'sortest_test_date' => '21-AUG-16',
            'sortest_test_score' => '78',
            'sortest_tsrc_code' => 'MUTX',
            'sortest_tadm_code' => '',
            'sortest_activity_date' => '30-AUG-16',
            'sortest_admr_code' => '',
            'sortest_tein_code' => '',
            'sortest_tefr_code' => '',
            'sortest_teac_code' => '',
            'sortest_tepr_code' => '',
            'sortest_rcrv_ind' => '',
            'sortest_sat_orig_score' =>'', 
            'sortest_term_code_entry' => '',
            'sortest_appl_no' => '',
            'sortest_instr_id' => '',
            'sortest_release_ind' => '',
            'sortest_equiv_ind' => 'N',
            'sortest_sat_essay_id' => '',
            'sortest_user_id' => '',
            'sortest_data_origin' =>'', 
            'sortest_surrogate_id' => '',
            'sortest_version' => '',
            'sortest_vpdi_code' => '',
            'szbuniq_unique_id' => 'UID'
                ),
            array(
            'sortest_pidm' => 12345678,
            'sortest_tesc_code' => 'GR02',
            'sortest_test_date' => '21-AUG-05',
            'sortest_test_score' => '780',
            'sortest_tsrc_code' => '',
            'sortest_tadm_code' => '',
            'sortest_activity_date' => '30-AUG-16',
            'sortest_admr_code' => '',
            'sortest_tein_code' => '',
            'sortest_tefr_code' => '',
            'sortest_teac_code' => '',
            'sortest_tepr_code' => '',
            'sortest_rcrv_ind' => '',
            'sortest_sat_orig_score' =>'', 
            'sortest_term_code_entry' => '',
            'sortest_appl_no' => '',
            'sortest_instr_id' => '',
            'sortest_release_ind' => '',
            'sortest_equiv_ind' => 'N',
            'sortest_sat_essay_id' => '',
            'sortest_user_id' => '',
            'sortest_data_origin' =>'', 
            'sortest_surrogate_id' => '',
            'sortest_version' => '',
            'sortest_vpdi_code' => '',
            'szbuniq_unique_id' => 'UID'
                ));
    }

    public function mockQueryOneResponse(){
        return array(
            array(
                "pidm"=> 12345678,
                "uniqueId"=> "UID",
                "testCode"=> "CM5",
                "testType"=> "MPT",
                "testDate"=> "2016-08-21",
                "testScore"=> "78",
                "tsrcCode"=> "MUTX",
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> '',
                "userID"=> '',
                "dataOrigin"=> ''
                ),
            array(
                "pidm"=> 12345678,
                "uniqueId"=> "UID",
                "testCode"=> "GR02",
                "testType"=> "GRE",
                "testDate"=> "2005-08-21",
                "testScore"=> "780",
                "tsrcCode"=> '',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> '',
                "userID"=> '',
                "dataOrigin"=> '',
                ));
    }

public function mockQueryMultiple(){
        return array(
            array(
            'sortest_pidm' => 123456,
            'sortest_tesc_code' => 'CM5',
            'sortest_test_date' => '21-AUG-16',
            'sortest_test_score' => '78',
            'sortest_tsrc_code' => 'MUTX',
            'sortest_tadm_code' => '',
            'sortest_activity_date' => '30-AUG-16',
            'sortest_admr_code' => '',
            'sortest_tein_code' => '',
            'sortest_tefr_code' => '',
            'sortest_teac_code' => '',
            'sortest_tepr_code' => '',
            'sortest_rcrv_ind' => '',
            'sortest_sat_orig_score' =>'', 
            'sortest_term_code_entry' => '',
            'sortest_appl_no' => '',
            'sortest_instr_id' => '',
            'sortest_release_ind' => '',
            'sortest_equiv_ind' => 'N',
            'sortest_sat_essay_id' => '',
            'sortest_user_id' => '',
            'sortest_data_origin' =>'', 
            'sortest_surrogate_id' => '',
            'sortest_version' => '',
            'sortest_vpdi_code' => '',
            'szbuniq_unique_id' => 'UID'
                ),
            array(
            'sortest_pidm' => 654321,
            'sortest_tesc_code' => 'GR02',
            'sortest_test_date' => '21-AUG-05',
            'sortest_test_score' => '780',
            'sortest_tsrc_code' => '',
            'sortest_tadm_code' => '',
            'sortest_activity_date' => '30-AUG-16',
            'sortest_admr_code' => '',
            'sortest_tein_code' => '',
            'sortest_tefr_code' => '',
            'sortest_teac_code' => '',
            'sortest_tepr_code' => '',
            'sortest_rcrv_ind' => '',
            'sortest_sat_orig_score' =>'', 
            'sortest_term_code_entry' => '',
            'sortest_appl_no' => '',
            'sortest_instr_id' => '',
            'sortest_release_ind' => '',
            'sortest_equiv_ind' => 'N',
            'sortest_sat_essay_id' => '',
            'sortest_user_id' => '',
            'sortest_data_origin' =>'', 
            'sortest_surrogate_id' => '',
            'sortest_version' => '',
            'sortest_vpdi_code' => '',
            'szbuniq_unique_id' => 'UID'
                ));
    }

    public function mockQueryMultipleResponse(){
        return array(
            array(
                "pidm"=> 123456,
                "uniqueId"=> "UID",
                "testCode"=> "CM5",
                "testType"=> "MPT",
                "testDate"=> "2016-08-21",
                "testScore"=> "78",
                "tsrcCode"=> "MUTX",
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> '',
                "userID"=> '',
                "dataOrigin"=> ''
                ),
            array(
                "pidm"=> 654321,
                "uniqueId"=> "UID",
                "testCode"=> "GR02",
                "testType"=> "GRE",
                "testDate"=> "2005-08-21",
                "testScore"=> "780",
                "tsrcCode"=> '',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> '',
                "userID"=> '',
                "dataOrigin"=> '',
                ));
    }

}
