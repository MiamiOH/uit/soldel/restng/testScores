<?php

namespace MiamiOH\RestngTestScores\Tests\Unit;

use MiamiOH\RESTng\App;

class PUTScoreTest extends \MiamiOH\RESTng\Testing\TestCase {


    private $dbh,$sth,$request, $response, $helper, $bannerUtil, $score, $queryallRecords,$person;


    protected function setUp() {

        $api = $this->createMock(App::class);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
                        ->setMethods(array('getData'))
                        ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
                ->setMethods(array('queryall_array','queryfirstcolumn','prepare'))
                ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
                ->setMethods(array('getHandle'))
                ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('bind_by_name','execute'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->willReturn(true);

        $this->sth->method('execute')
            ->willReturn(true);

        $this->dbh->method('prepare')->willReturn($this->sth);
      

        $this->score = new \MiamiOH\RestngTestScores\Services\Score();

        $this->score->setApp($api);
        $this->score->setDatabase($db);
        $this->score->setBannerUtil($this->bannerUtil);
        $this->score->setRequest($this->request);

        $this->helper = new \MiamiOH\RestngTestScores\Services\Helper();

        //mock the test type mapping
        $this->testTypeMap = array('MPT'=>array('MPTR','MPT1','CM2','CM5','CM7'),'GRE'=>array('GR01','GR02','GR05'));
        $this->helper->setTestTypeMap($this->testTypeMap);
        $this->helper->setDatabase($db);

        $this->score->setHelper($this->helper);

      
      }

    
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid unique ID.     
     */
    public function testPUTInvalidUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataInvalidUniqueID')));   
        
        $this->response  = $this->score->updateScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Unique ID is required.
     */
    public function testPUTNotSetUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataNotSetUniqueID')));
        
        $this->response  = $this->score->updateScore();

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid test code.     
     */
    public function testPUTInvalidTestCode() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataInvalidTestCode')));   
        
        $this->response  = $this->score->updateScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestCode is required.
     */
    public function testPUTNotSetTestCode() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataNotSetTestCode')));
        
        $this->response  = $this->score->updateScore();

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid test score.     
     */
    public function testPUTInvalidTestScore() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataInvalidTestScore')));   
        
        $this->response  = $this->score->updateScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestScore is required.
     */
    public function testPUTNotSetTestScore() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataNotSetTestScore')));
        
        $this->response  = $this->score->updateScore();

    }
     /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid equivalency indicator.     
     */
    public function testPUTInvalidEquivInd() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataInvalidEquivInd')));   
        
        $this->response  = $this->score->updateScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage EquivInd is required.
     */
    public function testPUTNotSetEquivInd() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataNotSetEquivInd')));
        
        $this->response  = $this->score->updateScore();

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Invalid testDate.     
     */
    public function testPUTInvalidTestDate() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataInvalidTestDate')));   
        
        $this->response  = $this->score->updateScore();        

    }
    /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage TestDate is required.
     */
    public function testPUTNotSetTestDate() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataNotSetTestDate')));
        
        $this->response  = $this->score->updateScore();

    }

 /**
     * @expectedException \MiamiOH\RESTng\Exception\BadRequest
     * @expectedExceptionMessage Unique ID does not exist.
     */

    public function testPUTNonExistUniqueID() {

        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataNonExistUniqueID')));

        $this->dbh->method('queryfirstcolumn')
               ->willReturn(\MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET);

        $this->response  = $this->score->updateScore();
       
    }



    //A successful PUT.
    public function testPUTSuccess(){
        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataValid')));
        
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryPUTResult')));


        $this->dbh->method('queryfirstcolumn')
               ->willReturn(123456);

        $this->response  = $this->score->updateScore();
       
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());      
        $this->assertEquals($this->response->getPayload(), $this->mockPUTResponse());

    }

    //PUT successful, but couldn't get the record back from SQL query
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Querying new record failed.
     */

    public function testPUTFailedQuery(){
        $this->request->method('getData')    
               ->will($this->returnCallback(array($this, 'getPUTDataValid')));
        
        $this->dbh->method('queryall_array')
             ->will($this->returnCallback(array($this, 'mockQueryPUTEmptyResult')));

        $this->dbh->method('queryfirstcolumn')
               ->willReturn(123456);

        $this->response  = $this->score->updateScore();

    }

    public function getPUTDataInvalidUniqueID(){
        return array( 
                "uniqueId"=> 'invalid_uid@#$%',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPUTDataNotSetUniqueID(){
        return array( 
                // "uniqueId"=> 'invalid_uid@#$%',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPUTDataInvalidTestCode(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'invalid_C*)',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPUTDataNotSetTestCode(){
        return array( 
                "uniqueId"=> 'someuid',
                // "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPUTDataInvalidTestScore(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '7899999',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
     public function getPUTDataNotSetTestScore(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                // "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPUTDataInvalidEquivInd(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "A", //invalid
                "satEssayID"=> ''                             
            );
    }
     public function getPUTDataNotSetEquivInd(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                // "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPUTDataInvalidTestDate(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-13-21',//invalid
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "Y", 
                "satEssayID"=> ''                             
            );
    }
     public function getPUTDataNotSetTestDate(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                // "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }

    public function getPUTDataValid(){
        return array( 
                "uniqueId"=> 'someuid',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }
    public function getPUTDataNonExistUniqueID(){
        return array( 
                "uniqueId"=> 'NOBODY',
                "testCode"=> 'CM5',                
                "testDate"=> '2016-08-21',
                "testScore"=> '78',
                "tsrcCode"=> 'MUTX',
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> ''                             
            );
    }

    public function mockQueryPUTResult(){
        return array(
            array(
            'sortest_pidm' => 123456,
            'sortest_tesc_code' => 'CM5',
            'sortest_test_date' => '21-AUG-16',
            'sortest_test_score' => '78',
            'sortest_tsrc_code' => 'MUTX',
            'sortest_tadm_code' => '',
            'sortest_activity_date' => '30-AUG-16',
            'sortest_admr_code' => '',
            'sortest_tein_code' => '',
            'sortest_tefr_code' => '',
            'sortest_teac_code' => '',
            'sortest_tepr_code' => '',
            'sortest_rcrv_ind' => '',
            'sortest_sat_orig_score' =>'', 
            'sortest_term_code_entry' => '',
            'sortest_appl_no' => '',
            'sortest_instr_id' => '',
            'sortest_release_ind' => '',
            'sortest_equiv_ind' => 'N',
            'sortest_sat_essay_id' => '',
            'sortest_user_id' => '',
            'sortest_data_origin' =>'TestScores API', 
            'sortest_surrogate_id' => '',
            'sortest_version' => '',
            'sortest_vpdi_code' => '',
            'szbuniq_unique_id' => 'someuid'
           ) );
    }
    
    public function mockQueryPUTEmptyResult(){
        return array();
    }

    public function mockPUTResponse(){
        return 
            array(
                "pidm"=> 123456,
                "uniqueId"=> "someuid",
                "testCode"=> "CM5",
                "testType"=> "MPT",
                "testDate"=> "2016-08-21",
                "testScore"=> "78",
                "tsrcCode"=> "MUTX",
                "tadmCode"=> '',
                "admrCode"=> '',
                "teinCode"=> '',
                "tefrCode"=> '',
                "teacCode"=> '',
                "teprCode"=> '',
                "rcrvInd"=> '',
                "satOrigScore"=> '',
                "termCodeEntry"=> '',
                "applNo"=> '',
                "instrID"=> '',
                "releaseInd"=> '',
                "equivInd"=> "N",
                "satEssayID"=> '',
                "userID"=> '',
                "dataOrigin"=> 'TestScores API'
            );
    }

}
